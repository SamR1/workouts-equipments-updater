# Workouts Updater

script to update workouts on [FitTrackee](https://samr1.github.io/FitTrackee)

---

## Prerequisite

- Python 3.9+
- SQlite
- FitTrackee v0.9+


previous version: branch [fittrackee_v0.8.x](https://codeberg.org/SamR1/workouts-updater/src/branch/fittrackee_v0.8.x)


## Installation

* clone this repository

* install Python dependencies:

```shell
$ make install
```

* init SQLite database:

```shell
$ make init-db
```

- create `.env` file from example and update FITTRACKEE_URL variable:
```shell
$ cp .env.example .env
```

## How-to

- create equipments on **FitTrackee** if needed

- create an application in **FitTrackee**, with the following scope: `equipments:read workouts:read workouts:write` (see [documentation](https://samr1.github.io/FitTrackee/en/oauth.html)). As there is no web application, the Application and Redirection URLs can be anything but must start with `https://`.

- after registration, update `.env` file with the client id and secret displayed on **FitTrackee**

- run the script, for example:
  - for equipments 
    - to update all running workouts (id: 5) with an equipment: 
    ```shell
    $ make update-workouts ARGS="-s 5 -e 6ZK5x8HKxQbdRYoPpTLKjf"
    ```
    - to update all hiking workouts (id: 3) with default equipment for hiking: 
    ```shell
    $ make update-workouts ARGS="-s 3"
    ```
    - to remove equipments on mountain biking workouts on a given period: 
    ```shell
    $ make update-workouts ARGS="-s 4 -e none --from 2024-01-01 --to 2024-02-01"
    ```
    
    It only updates workouts without equipments when `--force` is not provided.   
    If the equipment id is not specified, the workouts will be updated with default sport equipment.

  - to update visibilities for workouts returned in page 1: 
    ```shell
    $ make update-workouts ARGS="-av followers_only -mv private -wv public"
    ```
    **FitTrackee** API prevents visibility updates with invalid values. For instance with the following command:
    ```shell
    $ make update-workouts ARGS="-av followers_only -mv public -wv public"
    ```
    map visibility (`-mv`) will be `followers_only` and not `public` since analysis visibility (`-av`) is `followers_only` .

**Note**: before executing script, backup the database in case of a large number of workouts being updated. 
 
The script allows to complete the authorization flow by copying the authorization URL (inspired by [jat255/strava-to-fittrackee](https://github.com/jat255/strava-to-fittrackee)). The token is store in the SQLite database.

**Note**: if rate limits are enabled on **Fittrackee** instance, errors may be encountered if too many calls are made. 

- all options

```shell
$ make update-workouts ARGS="--help"
Usage: update_workouts.py [OPTIONS]

Options:
  -s, --sport_id INTEGER          id for sport
  -e, --equipment_id TEXT         equipment short id (will override sport
                                  default equipments). If 'none', it removes
                                  equipments (will override --force)
  -av, --analysis-visibility [public|followers_only|private]
                                  Visibility for analysis (chart).
  -mv, --map-visibility [public|followers_only|private]
                                  Visibility for map.
  -wv, --workout-visibility [public|followers_only|private]
                                  Visibility for workout.
  -f, --from TEXT                 start date (format: %Y-%m-%d)
  -t, --to TEXT                   end date (format: %Y-%m-%d)
  --title TEXT                    any part (or all) of the workout title;
                                  title matching is case-insensitive
  --notes TEXT                    any part (or all) of workout notes; notes
                                  matching is case-insensitive
  --description TEXT              any part (or all) of workout description;
                                  description matching is case-insensitive
  --per-page INTEGER              number of workouts per page
  --page INTEGER                  page number
  --order TEXT                    workout date order: asc, desc
  --force                         force update when workout is already
                                  associated with equipment
  --help                          Show this message and exit.

```