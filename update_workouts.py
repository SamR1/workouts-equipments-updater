import os
import sqlite3
import sys
from typing import Optional

import click
from requests_oauthlib import OAuth2Session

FITTRACKEE_URL = os.getenv("FITTRACKEE_URL", "")
FITTRACKEE_CLIENT_ID = os.getenv("FITTRACKEE_CLIENT_ID", "")
FITTRACKEE_SECRET = os.getenv("FITTRACKEE_SECRET", "")
DATABASE_URI = os.getenv("DATABASE_URI", "updater.db")

con = sqlite3.connect(DATABASE_URI)

VISIBILITY_LEVELS = ["public", "followers_only", "private"]


def validate_order(
    ctx: click.core.Context, param: click.core.Option, value: Optional[str]
) -> Optional[str]:
    if value and value not in ["asc", "desc"]:
        raise click.BadParameter("order must be 'asc' or 'desc'")
    return value


class WorkoutsUpdater:
    def __init__(self) -> None:
        self.fittrackee_url = FITTRACKEE_URL
        self.fittrackee_api_url = f"{FITTRACKEE_URL}/api"
        self.client_id = FITTRACKEE_CLIENT_ID
        self.client_secret = FITTRACKEE_SECRET
        self.scope = "equipments:read workouts:read workouts:write"
        self.oauth_session = None
        self.connection = sqlite3.connect(DATABASE_URI)

    def delete_token(self) -> None:
        cur = self.connection.cursor()
        cur.execute("DELETE FROM token;")
        self.connection.commit()

    def store_token(self, token: dict) -> None:
        self.delete_token()
        cur = self.connection.cursor()
        cur.execute(
            f"""
            INSERT INTO token (
                access_token, refresh_token, scope, token_type,
                expires_in, expires_at
            ) VALUES (
                '{token['access_token']}',
                '{token['refresh_token']}',
                '{self.scope}',
                '{token['token_type']}',
                {int(token['expires_in'])},
                {int(token['expires_at'])}
            );
            """
        )
        self.connection.commit()

    def get_session(self, token: dict) -> OAuth2Session:
        return OAuth2Session(
            self.client_id,
            token=token,
            auto_refresh_url=f"{self.fittrackee_api_url}/oauth/token",
            auto_refresh_kwargs={
                "client_id": self.client_id,
                "client_secret": self.client_secret,
            },
            scope=self.scope,
            token_updater=self.store_token,
        )

    def authorise_app(self) -> None:
        # adapted from https://github.com/jat255/strava-to-fittrackee
        self.oauth_session = OAuth2Session(self.client_id, scope=self.scope)
        authorization_url, _ = self.oauth_session.authorization_url(  # type: ignore
            f"{self.fittrackee_url}/profile/apps/authorize"
        )

        print(
            "\nPlease open the following URL to authorize the application:\n\n"
            f"{authorization_url}"
        )
        authorization_response = input(
            "\nand enter the URL displayed after application authorization "
            "and redirection to get authorization code:\n"
        )

        token = self.oauth_session.fetch_token(  # type: ignore
            f"{self.fittrackee_api_url}/oauth/token",
            include_client_id=True,
            client_secret=self.client_secret,
            authorization_response=authorization_response,
        )
        self.store_token(token)

    def get_token(self) -> None:
        cur = self.connection.cursor()
        row = cur.execute("SELECT * FROM token;").fetchone()
        if row:
            token = {
                "access_token": row[0],
                "refresh_token": row[1],
                "scope": row[2],
                "token_type": row[3],
                "expires_in": row[4],
                "expires_at": row[5],
            }
            self.oauth_session = self.get_session(token)
        else:
            self.authorise_app()

    def update_workouts(
        self,
        sport_id: int,
        equipment_id: Optional[str],
        analysis_visibility: Optional[str],
        map_visibility: Optional[str],
        workout_visibility: Optional[str],
        from_: Optional[str],
        to: Optional[str],
        title: Optional[str],
        description: Optional[str],
        notes: Optional[str],
        per_page: Optional[int],
        page: Optional[int],
        order: Optional[str],
        force: bool,
    ) -> None:
        if not self.oauth_session:
            click.echo("Error: no oauth session defined.")
            sys.exit(1)

        if equipment_id and not sport_id:
            click.echo("Error: equipment id must be provided with sport id.")
            sys.exit(1)

        updated_data = {}
        if analysis_visibility:
            updated_data["analysis_visibility"] = analysis_visibility
        if map_visibility:
            updated_data["map_visibility"] = map_visibility
        if workout_visibility:
            updated_data["workout_visibility"] = workout_visibility

        if not sport_id and not updated_data:
            click.echo("No values to update, exiting.")
            sys.exit(0)

        sport = None
        equipment_ids = None
        if sport_id:
            click.echo("\nChecking sport...")
            r = self.oauth_session.get(
                f"{self.fittrackee_api_url}/sports/{sport_id}",
                headers=dict(content_type="application/json"),
            )

            if r.status_code == 401:
                # token is invalid
                self.delete_token()
                click.echo(
                    "invalid credentials, please retry or check config. exiting."
                )
                sys.exit(1)

            r.raise_for_status()

            sports = r.json().get("data", {}).get("sports", [])
            if not sports:
                click.echo("error when getting sport. exiting.")
                sys.exit(1)

            sport = sports[0]

        if equipment_id:
            if equipment_id == "none":
                force = True
                equipment_ids = []
            elif equipment_id == "default":
                equipments = sport.get("default_equipments")
                if not equipments:
                    click.echo("no default equipments. exiting...")
                    return
                equipments_count = len(equipments)
                click.echo(
                    f"{equipments_count} equipment"
                    f"{'' if equipments_count == 1 else 's'} found."
                )
                equipment_ids = [e["id"] for e in equipments]
            else:
                click.echo("checking equipment...")
                r = self.oauth_session.get(
                    f"{self.fittrackee_api_url}/equipments/{equipment_id}",
                    headers=dict(content_type="application/json"),
                )
                if r.status_code == 200:
                    click.echo("> equipment OK")
                else:
                    click.echo("> invalid equipment")
                    return

                equipment_ids = [equipment_id]

        if not equipment_ids and equipment_id != "none" and not updated_data:
            click.echo(
                f"No default equipment for sport '{sport.get("label")}' (id: {sport_id}), exiting."
            )
            sys.exit(0)

        updated_data["equipment_ids"] = equipment_ids

        params = ""
        if sport_id:
            params += f"&sport_id={sport_id}"
        if from_:
            params += f"&from={from_}"
        if to:
            params += f"&to={to}"
        if title:
            params += f"&title={title}"
        if notes:
            params += f"&notes={notes}"
        if description:
            params += f"&description={description}"
        if per_page:
            params += f"&per_page={per_page}"
        if page:
            params += f"&page={page}"
        if order:
            params += f"&order={order}"

        click.echo("\nGetting workouts to update...")
        r = self.oauth_session.get(
            f"{self.fittrackee_api_url}/workouts?return_equipments=true{params}",
            headers=dict(content_type="application/json"),
        )

        if r.status_code == 401:
            # token is invalid
            self.delete_token()
            click.echo(
                "invalid credentials, please retry or check config. exiting."
            )
            sys.exit(1)

        r.raise_for_status()

        workouts = r.json().get("data", {}).get("workouts", [])

        if not workouts:
            click.echo("No workouts found, exiting.")
            sys.exit(0)

        workouts_count = len(workouts)
        click.echo(
            f"{workouts_count} workout"
            f"{'' if workouts_count == 1 else 's'} found."
        )

        for workout in workouts:
            click.echo(
                f"Updating workout '{workout['id']} ({workout['title']})'"
            )

            if (
                equipment_ids is not None
                and workout.get("equipments")
                and not force
            ):
                click.echo(
                    f"> workout '{workout['id']}' already with equipments!"
                )
                continue

            r = self.oauth_session.patch(
                f"{self.fittrackee_api_url}/workouts/{workout['id']}",
                json=updated_data,
                headers=dict(content_type="application/json"),
            )

            if r.status_code == 200:
                click.echo("> workout updated!")
            else:
                error_message = r.json().get("message", "unknown error")
                click.echo(f"> error {r.status_code}: {error_message}")
                sys.exit(1)

        click.echo("\ndone.")


@click.command()
@click.option(
    "--sport_id",
    "-s",
    help="id for sport",
    type=int,
)
@click.option(
    "--equipment_id",
    "-e",
    help=(
        "equipment short id (will override sport default equipments). "
        "If 'none', it removes equipments (will override --force)"
    ),
    type=str,
)
@click.option(
    "--analysis-visibility",
    "-av",
    type=click.Choice(VISIBILITY_LEVELS),
    help="Visibility for analysis (chart).",
)
@click.option(
    "--map-visibility",
    "-mv",
    type=click.Choice(VISIBILITY_LEVELS),
    help="Visibility for map.",
)
@click.option(
    "--workout-visibility",
    "-wv",
    type=click.Choice(VISIBILITY_LEVELS),
    help="Visibility for workout.",
)
@click.option(
    "--from",
    "-f",
    "from_",
    help="start date (format: %Y-%m-%d)",
)
@click.option(
    "--to",
    "-t",
    help="end date (format: %Y-%m-%d)",
)
@click.option(
    "--title",
    help=(
        "any part (or all) of the workout title; title matching is case-insensitive"
    ),
)
@click.option(
    "--notes",
    help=(
        "any part (or all) of workout notes; notes matching is case-insensitive"
    ),
)
@click.option(
    "--description",
    help=(
        "any part (or all) of workout description; description matching is case-insensitive"
    ),
)
@click.option(
    "--per-page",
    help="number of workouts per page",
    type=int,
)
@click.option("--page", help="page number", type=int)
@click.option(
    "--order",
    help="workout date order: asc, desc",
    type=str,
    callback=validate_order,
)
@click.option(
    "--force",
    help="force update when workout is already associated with equipment",
    is_flag=True,
    show_default=True,
    default=False,
)
def update_workouts(
    sport_id: int,
    equipment_id: Optional[str],
    analysis_visibility: Optional[str],
    map_visibility: Optional[str],
    workout_visibility: Optional[str],
    from_: Optional[str],
    to: Optional[str],
    title: Optional[str],
    description: Optional[str],
    notes: Optional[str],
    per_page: Optional[int],
    page: Optional[int],
    order: Optional[str],
    force: bool,
) -> None:
    visibilities = (
        str(analysis_visibility)
        + str(map_visibility)
        + str(workout_visibility)
    )
    if visibilities != "NoneNoneNone" and "None" in visibilities:
        click.echo("Error: all visibilities must be provided together.")
        sys.exit(1)

    workouts_updater = WorkoutsUpdater()
    workouts_updater.get_token()
    workouts_updater.update_workouts(
        sport_id,
        equipment_id,
        analysis_visibility,
        map_visibility,
        workout_visibility,
        from_,
        to,
        title,
        description,
        notes,
        per_page,
        page,
        order,
        force,
    )


if __name__ == "__main__":
    if not FITTRACKEE_URL or not FITTRACKEE_CLIENT_ID or not FITTRACKEE_SECRET:
        click.echo(
            "missing environment variables, please check .env file", err=True
        )
        sys.exit(1)

    update_workouts()
