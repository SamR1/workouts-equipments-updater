import os
import sqlite3

DATABASE_URI = os.getenv("DATABASE_URI", "updater.db")

con = sqlite3.connect(DATABASE_URI)
cur = con.cursor()

cur.execute(
    """
    CREATE TABLE IF NOT EXISTS token(
    access_token TEXT PRIMARY KEY NOT NULL UNIQUE,
    refresh_token TEXT,
    scope TEXT DEFAULT '',
    token_type TEXT,
    expires_in INTEGER NOT NULL DEFAULT 0,
    expires_at INTEGER NOT NULL DEFAULT 0
    );
"""
)

cur.execute(
    """
    CREATE INDEX IF NOT EXISTS token_access_token ON token (access_token);
"""
)

cur.execute(
    """
    CREATE INDEX IF NOT EXISTS token_refresh_token ON token (refresh_token);
"""
)
