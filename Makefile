include Makefile.config
-include .env
.SILENT:

clean:
	rm -fr $(VENV)

check:
	$(RUFF) check

checks: fix format type-check

fix:
	echo -e "\nchecking and fixing files..."
	$(RUFF) check --fix

format:
	echo -e "\nformatting files..."
	$(RUFF) format

init-db:
	echo -e '\ninitialise database...'
	$(PYTHON) init_db.py
	echo -e 'done.'

install: venv
	$(PIP) install -r requirements.txt

install-dev: venv
	$(PIP) install -r requirements-dev.txt

type-check:
	echo -e '\nrunning mypy...'
	$(MYPY) .

update-workouts:
	$(PYTHON) -m update_workouts $(ARGS)

venv:
	test -d $(VENV) || $(PYTHON_VERSION) -m venv $(VENV)
	$(PIP) install -U pip setuptools